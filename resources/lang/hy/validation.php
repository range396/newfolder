<?php
return [
    'required' => 'Դաշտը պարտադիր է',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => ':attribute Դաշտը պետք է պարունակի :min ից ավել սիմվոլներ ',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'regex' => 'Ներմուծված :attribute դաշտը պարտկեշտ չէ',
    'email' => ':attribute դաշտում ներմուծված են ոչ պարտկեշտ տվիալներ',





    /*
   |--------------------------------------------------------------------------
   | Custom Validation Language Lines
   |--------------------------------------------------------------------------
   |
   | Here you may specify custom validation messages for attributes using the
   | convention "attribute.rule" to name the lines. This makes it quick to
   | specify a specific custom language line for a given attribute rule.
   |
   */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        "phoneContact" => "հեռախոսի",
        "emailContact" => "էլ․ հասցեի"
    ],

];