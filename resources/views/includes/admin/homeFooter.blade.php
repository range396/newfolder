<footer class="adminfooter">
    <div class="container">
        <div class="copy text-center">
            &copy; Copyright 2020 <a href='#'>New Folder</a>
        </div>
    </div>
</footer>

<script src="/assets/js/jquery1.1.js"></script>

<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
{{--<script src="/assets/vendors/select/bootstrap-select.js"></script>--}}
<script src="/assets/vendors/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
<script src="/assets/vendors/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js"></script>
<script src="/assets/vendors/ckeditor/ckeditor.js"></script>
<script src="/assets/vendors/ckeditor/adapters/jquery.js"></script>
<script src="/assets/js/custom.js"></script>
<script src="/assets/js/editors.js"></script>

<script>

    jQuery.noConflict();
    jQuery(document).ready(function ($) {
        let token = "{{ csrf_token() }}";

        /* HOME PAGE */
        let _URL = window.URL || window.webkitURL;
        $(document).on('change', "#pageLogo", function () {
            let img, file;
            let width;
            let height;
            if ((file = this.files[0])) {
                img = new Image();
                img.onload = function() {
                    width = this.width;
                    height = this.height;
                    if(width <= 260 && height <= 100 && file.size < 2000000) {
                        $(".homeHead form#logoForm").submit();
                        $(".homeHead form#logoForm #pageLogo").val('');
                    }else {
                        alert("{{ trans("settings.websiteLogoValidation") }}");
                        $(".homeHead form#logoForm #pageLogo").val('');
                        $(".homeHead form#logoForm").submit(false);
                        return false;
                    }
                    console.log(width, height, file.size)
                };
                img.onerror = function() {
                    alert("{{ trans("settings.websiteLogoValidation") }} - " + file.type);
                };
                img.src = _URL.createObjectURL(file);
                return true;
            }
        });

        let language = '{{ app()->getLocale() }}';
        var editorFirst = $('textarea#homeEditor1_{{Request::segment(1)}}').ckeditor({
            language: language,
        }).editor;

        var firstHomeEditor = $('textarea#homeEditor1_{{Request::segment(1)}}');
        var homeHeaderTitle = $('.homeHeaderPostTitle');

        const errors = {};
        var homeHeaderPostId;
        // let homeDelHeaderBtn = $(".deleteHeader.deleteContainer");
        let homeDelBtnHead = $(".deleteHeader.deleteContainer");
        let homeDelBtnMain = $(".deleteMain.deleteContainer");

        var pageName = '{{Request::segment(3)}}';

        function lockMaxLength(edt, len) {
            let evts = ['paste', 'key', 'input'];
            let locked;
            for (ev of evts) {
                edt.on(ev, function (evt) {
                    var currentLength = edt.getData().length,
                        maximumLength = len;
                    if (currentLength >= maximumLength) {
                        if (!locked) {
                            // Record the last legal content.
                            edt.fire('saveSnapshot');
                            locked = 1;
                            // Cancel the keystroke.
                            evt.cancel();
                        } else
                        // Check after this key has effected.
                            setTimeout(function () {
                                // Rollback the illegal one.
                                if (edt.getData().length > maximumLength) {
                                    edt.execCommand('undo');
                                    if (edt.getData().length > maximumLength) {
                                        edt.setData(edt.getData().substring(0, maximumLength));
                                    }
                                    alert(" {{ __("settings.maxLengthPost") }} ");
                                } else {
                                    locked = 0;
                                }
                            }, 0);
                    }
                    this.timestamp = 'ABCD';
                });
            }
        }

        lockMaxLength(editorFirst, 480);

        editorFirst.on('instanceReady', function (ev) {
            // console.log(ev.editor.getData().replace(/<[^>]*>|\s/g, '').length)
            this.dataProcessor.writer.indentationChars = '\t';
            this.dataProcessor.writer.lineBreakChars = '\n';
            this.dataProcessor.writer.setRules('p', {
                indent: true,
                breakBeforeOpen: false,
                breakAfterOpen: false,
                breakBeforeClose: false,
                breakAfterClose: true
            });

            $.ajax({
                url: 'checkP',
                method: "POST",
                data: {"_token": token, 'editorName': editorFirst.id, "pageName": pageName},
                cache: false,
                success: function (dat) {
                    homeHeaderPostId = dat.postId;
                    homeDelBtnHead.css({"display": "inline-block"});
                    homeDelBtnHead.data('id', dat.postId);
                    // homeDelHeaderBtn.css({"display":"inline-block"});
                    // homeDelHeaderBtn.data('id',dat.postId);
                    $.ajax({
                        url: 'lastP',
                        method: "POST",
                        data: {"_token": token, "currentPostId": dat.postId}, //localStorage.getItem('lastPostId') },
                        cache: false,
                        success: function (d) {
                            if (language == 'ru') {
                                $('textarea#homeEditor1_{{Request::segment(1)}}').val(d.lastPost.contentRu);
                                homeHeaderTitle.data('lang', '{{Request::segment(1)}}').val(d.lastPost.nameRu);
                            } else if (language == 'en') {
                                $('textarea#homeEditor1_{{Request::segment(1)}}').val(d.lastPost.contentEn);
                                homeHeaderTitle.data('lang', '{{Request::segment(1)}}').val(d.lastPost.nameEn);
                            } else {
                                $('textarea#homeEditor1_{{Request::segment(1)}}').val(d.lastPost.content);
                                homeHeaderTitle.data('lang', '{{Request::segment(1)}}').val(d.lastPost.name);
                            }
                            if (d.lastPost.type == editorFirst.id) {
                                $(".headPostImage img").attr('src', d.lastPost.image);
                            }
                        }
                    }).error(function (err, status) {
                    })
                }
            }).error(function (err, status) {
                localStorage.removeItem('headerHomeLastPId');
            });

            if (typeof homeDelBtnHead != "undefined") {
                homeDelBtnHead.on("click touchstart", function (ev) {
                    ev.preventDefault();
                    ev.stopPropagation();

                    $.ajax({
                        url: "deletePost",
                        method: "POST",
                        data: {
                            "_token": token,
                            "delId": homeDelBtnHead.data('id')
                        },
                        cache: false,
                        success: function (d) {
                            $(".homeHead .headPostImage img").attr("src","");
                            console.log(d)
                        }
                    }).error(function (err, status, error) {
                        console.log(error);
                    });

                });
            }

            // console.log(localStorage.getItem('lastPostId'));
        });

        homeHeaderTitle.on('input', function () {
            errors.homeHeaderTitle = "";
            homeHeaderTitle.css({"border": "1px solid green"});
        });

        let headformData = new FormData();
        $(document).on('change', "#headPostImage", function () {
            let img = document.getElementById('headPostImage').files[0];
            let imgName = img.name;
            let extentsion = imgName.split('.').pop().toLowerCase();
            if (jQuery.inArray(extentsion, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                alert("{{trans('settings.imageextErr')}} - gif, png, jpg, jpeg");
            }

            let imageSize = img.size;
            if (imageSize > 2000000) {
                alert("meca");
            } else {
                headformData.set("image", document.getElementById('headPostImage').files[0]);
                // console.log(headformData);
            }
        });


        let homeCatName;
        editorFirst.on("afterCommandExec", function (event) {
            var commandName = event.data.name;
            let data = $('textarea#homeEditor1_{{Request::segment(1)}}').val();

            if (homeHeaderTitle.val() == "") {

                $(".content-box.notice").addClass("show");
                $(".content-box.notice.show").css({"border-color":"red", "color":"red"});
                $(".content-box.notice").text("{{ trans("settings.error_message") }}");
                setTimeout(function() {
                    $(".content-box.notice").removeClass("show");
                }, 3000);

                homeHeaderTitle.css({"border": "1px solid red"});
                return false;
            }

            if (firstHomeEditor.val() == "") {
                return false;
            }

            headformData.set("_token", token);
            headformData.set("contentText", data);
            headformData.set("title", homeHeaderTitle.val());
            headformData.set("pageName", pageName);
            headformData.set("lang", language);
            headformData.set("categoryName", "header_content");
            headformData.set("typeName", editorFirst.id);
            headformData.set("pId", homeHeaderPostId);

            if (commandName == 'save') {

                // console.log(headformData);
                let homePostHeadUrl = homeHeaderPostId > 0 && typeof homeHeaderPostId != undefined && typeof homeHeaderPostId != null ? 'pUpdate' : 'pSave';
                let editorId = editorFirst.id;
                $.ajax({
                    url: homePostHeadUrl,
                    method: "POST",
                    data: headformData,
                    //{
                    {{--"_token": token,--}}
                    {{--"contentText": data,--}}
                    {{--"title": homeHeaderTitle.val(),--}}
                    {{--"pageName": pageName,--}}
                    {{--headformData,--}}
                    {{--"lang": language,--}}
                    {{--"categoryName": "header_content",--}}
                    {{--"typeName": editorId,--}}
                    {{--"pId": homeHeaderPostId,--}}
                    //},
                    cache: false,
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        // Get Latest post
                        homeHeaderPostId = res.lastId;
                        homeHeaderCatName = res.catName;
                        $(".headPostImage img").attr('src', res.ImgPath);
                        $(".content-box.notice").addClass("show");
                        $(".content-box.notice.show").css({"border-color":"green", "color":"green"});
                        $(".content-box.notice").text("{{ trans("settings.success_save") }}");
                        setTimeout(function() {
                            $(".content-box.notice").removeClass("show");
                        }, 3000);
                        for (var key in Object(headformData).entries()) {
                            headformData.delete(key)
                        }
                    }
                }).error(function (err, error) {
                });
                //console.log($('textarea#ckeditor_full').val());
            }
        });


        // Section Social Links Header


        /*$(".panel-body .linksList .item .link input[type='url']").on("keypress", function(e) {
            if(e.keyCode == 32) {
                return false;
            }
        });

        $(".panel-body .linksList .item .link input[type='url']").on("input", function() {

           if($(this).parent('.fb').length > 0) {
               $(this).parent('.fb').parent('.form-group').siblings('.image').children('svg').children('path').attr("fill","#1945ff");
               if($(this).val().length == 0){
                   $(this).parent('.fb').parent('.form-group').siblings('.image').children('svg').children('path').attr("fill",'#dedede');
               }
           }

           if($(this).parent('.inst').length > 0) {
               // alert("ok inst");
           }
           if($(this).parent('.linkd').length > 0) {
               // alert("ok linkd");
           }

        })

        // let  hasHeadLinks = null;
        let linksHeadUrl;

        localStorage.removeItem("hasHeadLinks");
        $.ajax({
            url: "checkSocial",
            method: "POST",
            data: {
{{--"_token": token,--}}
        "type": "socialLinksHead",
    },
    cache: false,
    success: function (resp) {
        resp.forEach(function (item, step) {
            $(".homeHead .linksList .item").eq(step).children('.form-group').children('.link').data('id', item.id);
            $(".homeHead .linksList .item").eq(step).children(".form-group").children('.link').children('input').val(item.name);
        });
        // hasHeadLinks = true;
        localStorage.setItem("hasHeadLinks", true);
    }

    }).error(function (err, status, error) {
    // hasHeadLinks = false;
    });


    $(".linksList .item input[type='url']").on("input", function() {
    $(this).css({"border-color":"inherit"});
    });

    let countheader = 0;
    $(".homeHead .linksList .item.saveLinks button").on("click touchstart", function () {
    countheader++;

    if (localStorage.getItem("hasHeadLinks") == "true") {
        linksHeadUrl = "updateLinks";
    } else {
        linksHeadUrl = "addLinks";
    }

    let socialLinksData = {
        "fbLink": $(".homeHead .linksList .item.fb .form-group").children('.link').children('input').val(),
        "fbId": $(".homeHead .linksList .item.fb .form-group").children('.link').data('id'),
        "fbIcon": $(".homeHead .linksList .item.fb").children('.image').html(),
        "instLink": $(".homeHead .linksList .item.inst .form-group").children('.link').children('input').val(),
        "instId": $(".homeHead .linksList .item.inst .form-group").children('.link').data('id'),
        "instIcon": $(".homeHead .linksList .item.inst").children('.image').html(),
        "linkdLink": $(".homeHead .linksList .item.linkd .form-group").children('.link').children('input').val(),
        "linkdId": $(".homeHead .linksList .item.linkd .form-group").children('.link').data('id'),
        "linkdIcon": $(".homeHead .linksList .item.linkd").children('.image').html(),
        "pageName": pageName,
        "type": "socialLinksHead"
    };

    if (socialLinksData.fbLink == "" && socialLinksData.instLink == "" && socialLinksData.linkdLink == "" && countheader == "1") {

        $.ajax({
            url: 'getSocialError',
            method: "POST",
            data: {
{{--"_token": token,--}}
        'linkFb': socialLinksData.fbLink,
        'linkInst': socialLinksData.instLink,
        'linkLinkd': socialLinksData.linkdLink
    },
    cache: false,
    success: function (d) {
        if (d.socialError.length > 0) {
            localStorage.setItem("hasFooterLinks", false);
            $(".socialErrorHead").html(d.socialError);
            // console.log(d.socialError);
            $(".linksList .item input[type='text']").css({"border-color":"red"});
        }
    }
    }).error(function (err, status, error) {

    });

    } else {
    $.ajax({
    url: linksHeadUrl,
    method: "POST",
    data: {
{{--"_token": token,--}}
        socialLinksData
    },
    cache: false,
    success: function (res) {
        localStorage.setItem("hasHeadLinks", true);
        res.linkInfo.forEach(function (item, step) {
            $(".homeHead .linksList .item").eq(step).children('.form-group').children('.link').data('id', item.id);
            $(".homeHead .linksList .item").eq(step).children('.form-group').children('.link').children('input').val(item.name);
        });
    }
    }).error(function (err, status, error) {

    });

    let ids = [];
    if (socialLinksData.fbLink == "") {
    ids.push(socialLinksData.fbId);
    }
    if (socialLinksData.instLink == "") {
    ids.push(socialLinksData.instId);
    }
    if (socialLinksData.linkdLink == "") {
    ids.push(socialLinksData.linkdId);
    }

    if (ids.length > 0) {
    $.ajax({
        url: "deleteSocLink",
        method: "POST",
{{--data: {"_token": token, "itemsId": ids},--}}
        cache: false,
        success: function (resp) {
            localStorage.setItem("hasHeadLinks", false);
            ids.length = 0;
        }
    }).error(function (err, status, error) {

    });
    }

    }

    });*/


        // Main section Post

        let homeSecondEditor = $('textarea#homeEditor2_{{Request::segment(1)}}');
        let homeMainTitle = $('.homeMainPostTitle');

        let homeMainPostId;

        let homeEditorTwo = $('textarea#homeEditor2_{{Request::segment(1)}}').ckeditor({
            language: '{{ app()->getLocale() }}',
        }).editor;

        lockMaxLength(homeEditorTwo, 670);
        homeEditorTwo.on('instanceReady', function () {
            // checkAndGet("textarea#homeEditor2_",homeMainPostId, homeEditorTwo, homeMainTitle);

            $.ajax({
                url: 'checkP',
                method: "POST",
                data: {"_token": token, 'editorName': homeEditorTwo.id, "pageName": pageName},
                cache: false,
                success: function (data) {
                    homeMainPostId = data.postId;
                    homeDelBtnMain.css({"display": "inline-block"});
                    homeDelBtnMain.data('id', data.postId);
                    $.ajax({
                        url: 'lastP',
                        method: "POST",
                        data: {"_token": token, "currentPostId": data.postId}, //localStorage.getItem('lastPostId') },
                        cache: false,
                        success: function (d) {
                            if (language == 'ru') {
                                $('textarea#homeEditor2_{{Request::segment(1)}}').val(d.lastPost.contentRu);
                                homeMainTitle.data('lang', '{{Request::segment(1)}}').val(d.lastPost.nameRu);
                            } else if (language == 'en') {
                                $('textarea#homeEditor2_{{Request::segment(1)}}').val(d.lastPost.contentEn);
                                homeMainTitle.data('lang', '{{Request::segment(1)}}').val(d.lastPost.nameEn);
                            } else {
                                $('textarea#homeEditor2_{{Request::segment(1)}}').val(d.lastPost.content);
                                homeMainTitle.data('lang', '{{Request::segment(1)}}').val(d.lastPost.name);
                            }
                            if (d.lastPost.type == homeEditorTwo.id) {
                                $(".mainPostImage img").attr('src', d.lastPost.image);
                            }

                        }
                    }).error(function (err, status) {
                        location.reload();
                    })
                }
            }).error(function (err, status) {
                localStorage.removeItem('lastPostId');
            });

            if (typeof homeDelBtnMain != "undefined") {
                homeDelBtnMain.children('a').on("click touchstart", function (ev) {
                    ev.preventDefault();
                    ev.stopPropagation();

                    $.ajax({
                        url: "deletePost",
                        method: "POST",
                        data: {
                            "_token": token,
                            "delId": homeDelBtnMain.data('id')
                        },
                        cache: false,
                        success: function (d) {
                            $(".homeMainSection .mainPostImage img").attr("src","");
                            homeMainTitle.val("");
                            console.log(d);
                        }
                    }).error(function (err, status, error) {
                        console.log(error);
                    });

                });
            }
            // console.log(localStorage.getItem('lastPostId'));
        });

        homeMainTitle.on('input', function () {
            errors.homeMainTitle = "";
            homeMainTitle.css({"border": "1px solid green"});
        });

        let mainformData = new FormData();
        $(document).on('change', "#mainPostImage", function () {
            let imgMain = document.getElementById('mainPostImage').files[0];
            let imgMainName = imgMain.name;
            let extentsion = imgMainName.split('.').pop().toLowerCase();
            if (jQuery.inArray(extentsion, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                alert("{{trans('settings.imageextErr')}} - gif, png, jpg, jpeg");
            }
            let imageSize = imgMain.size;
            if (imageSize > 2000000) {
                alert("meca");
            } else {
                mainformData.set("image", document.getElementById('mainPostImage').files[0]);
                // console.log(headformData);
            }
        });


        let homeMainCatName;

        homeEditorTwo.on("afterCommandExec", function (event) {

            let editorMainId = homeEditorTwo.id;
            var commandName = event.data.name;
            let dataMain = $('textarea#homeEditor2_{{Request::segment(1)}}').val();

            if (homeMainTitle.val() == "") {
                $(".content-box.notice").addClass("show");
                $(".content-box.notice.show").css({"border-color":"red", "color":"red"});
                $(".content-box.notice").text("{{ trans("settings.error_message") }}");
                setTimeout(function() {
                    $(".content-box.notice").removeClass("show");
                }, 3000);
                homeMainTitle.css({"border": "1px solid red"});
                return false;
            } else {
                errors.homeMainTitle = "";
            }

            if (homeSecondEditor.val() == "") {
                return false;
            }

            mainformData.set("_token", token);
            mainformData.set("contentText", dataMain);
            mainformData.set("title", homeMainTitle.val());
            mainformData.set("pageName", pageName);
            mainformData.set("lang", language);
            mainformData.set("categoryName", "main_content");
            mainformData.set("typeName", editorMainId);
            mainformData.set("pId", homeMainPostId);


            if (commandName == 'save') {
                let homeMainPostURL = homeMainPostId > 0 && typeof homeMainPostId != "undefined" && typeof homeMainPostId != "null" ? '/{{ app()->getLocale() }}/admin/pUpdate' : '/{{ app()->getLocale() }}/admin/pSave';

                $.ajax({
                    url: homeMainPostURL,
                    method: "POST",
                    data: mainformData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        // Get Latest post

                        // localStorage.setItem('mainHomeLastPId', );

                        $(".mainPostImage img").attr('src', res.ImgPath);
                        homeMainPostId = res.lastId;
                        homeMainCatName = res.catName;
                        $(".content-box.notice").addClass("show");
                        $(".content-box.notice.show").css({"border-color":"green", "color":"green"});
                        $(".content-box.notice").text("{{ trans("settings.success_save") }}");
                        setTimeout(function() {
                            $(".content-box.notice").removeClass("show");
                        }, 3000);

                        for (var key in Object(mainformData).entries()) {
                            mainformData.delete(key)
                        }
                        setTimeout(function(){
                            location.reload();
                        }, 2000);
                    }
                }).error(function (err, status, error) {
                    // console.log(err);
                });
                //console.log($('textarea#ckeditor_full').val());
            }

        });


        // Section Social Links Footer

        // let  hasFooterLinks = null;
        /*let linksFooterUrl;

        localStorage.removeItem("hasFooterLinks");
        $.ajax({
            url: "checkSocial",
            method: "POST",
            data: {
{{--"_token": token,--}}
        "type": "socialLinksFooter",
    },
    cache: false,
    success: function (resp) {
        resp.forEach(function (item, step) {
            $(".homeFooter .linksList .item").eq(step).children('.form-group').children('.link').data('id', item.id);
            $(".homeFooter .linksList .item").eq(step).children('.form-group').children('.link').children('input').val(item.name);
        });

        localStorage.setItem("hasFooterLinks", true);
    }
    }).error(function (err, status, error) {

    });

    let countfooter = 0;
    $(".homeFooter .linksList .item.saveLinks button").on("click touchstart", function () {
    countfooter++;

    if (localStorage.getItem("hasFooterLinks") == "true") {
        linksFooterUrl = "updateLinks";
    } else {
        linksFooterUrl = "addLinks";
    }

    let socialLinksData = {
        "fbLink": $(".homeFooter .linksList .item.fb .form-group").children('.link').children('input').val(),
        "fbId": $(".homeFooter .linksList .item.fb .form-group").children('.link').data('id'),
        "fbIcon": $(".homeFooter .linksList .item.fb").children('.image').html(),
        "instLink": $(".homeFooter .linksList .item.inst .form-group").children('.link').children('input').val(),
        "instId": $(".homeFooter .linksList .item.inst .form-group").children('.link').data('id'),
        "instIcon": $(".homeFooter .linksList .item.inst").children('.image').html(),
        "linkdLink": $(".homeFooter .linksList .item.linkd .form-group").children('.link').children('input').val(),
        "linkdId": $(".homeFooter .linksList .item.linkd .form-group").children('.link').data('id'),
        "linkdIcon": $(".homeFooter .linksList .item.linkd").children('.image').html(),
        "pageName": pageName,
        "type": "socialLinksFooter"
    };

    if (socialLinksData.fbLink == "" && socialLinksData.instLink == "" && socialLinksData.linkdLink == "" && countfooter == "1") {

        $.ajax({
            url: 'getSocialError',
            method: "POST",
            data: {
{{--"_token": token,--}}
        'linkFb': socialLinksData.fbLink,
        'linkInst': socialLinksData.instLink,
        'linkLinkd': socialLinksData.linkdLink
    },
    cache: false,
    success: function (d) {
        if (d.socialError.length > 0) {
            $(".socialErrorFooter").html(d.socialError);
            localStorage.setItem("hasFooterLinks", false);
        }
    }
    }).error(function (err, status, error) {

    });

    } else {

    $.ajax({
    url: linksFooterUrl,
    method: "POST",
    data: {
{{--"_token": token,--}}
        socialLinksData
    },
    cache: false,
    success: function (response) {
        localStorage.setItem("hasFooterLinks", true);
        response.linkInfo.forEach(function (item, step) {
            $(".homeFooter .linksList .item").eq(step).children('.form-group').children('.link').data('id', item.id);
            $(".homeFooter .linksList .item .form-group").eq(step).children('.link').children('input').val(item.name);

            console.log($(".homeFooter .linksList .item").eq(step).find('.form-group').children('.link').data('id'));
        });

    }
    }).error(function (err, status, error) {

    });

    let ids = [];
    if (socialLinksData.fbLink == "") {
    ids.push(socialLinksData.fbId);
    }
    if (socialLinksData.instLink == "") {
    ids.push(socialLinksData.instId);
    }
    if (socialLinksData.linkdLink == "") {
    ids.push(socialLinksData.linkdId)
    }

    if (ids.length > 0) {
    $.ajax({
        url: "deleteSocLink",
        method: "POST",
{{--data: {"_token": token, "itemsId": ids},--}}
        cache: false,
        success: function (resp) {
            localStorage.setItem("hasFooterLinks", false);
            ids.length = 0;
        }
    }).error(function (err, status, error) {

    });
    }
    }
    });*/


        // Section Services

        let homeServicesUrl;
        let dataInsert;
        $.ajax({
            url: "checkList",
            method: "POST",
            data: {"_token": token, "type": "services"},
            cache: false,
            success: function (d) {
                let arr = d.reverse();
                if (d.length == 1) {
                    $(".multipleContent").eq(0).attr("data-id", arr[0].id)
                    $(".multipleContent").eq(0).find(".serviceTitle").val(arr[0].name);
                }
                for (let i = 1; i < arr.length; i++) {
                    $("#homeServicesForm").prepend("<div class='multipleContent'>" + $("#homeServicesForm .multipleContent").html() + "</div>");
                    let time = setTimeout(function () {
                        $(".multipleContent").eq(0).attr("data-id", arr[0].id);
                        $(".multipleContent").eq(0).attr("data-count", 0);
                        if(arr[0].image && arr[i].image)
                            $(".multipleContent").eq(0).find(".okIconContainer").css({"visibility":"visible"});
                        $(".multipleContent").eq(i).find(".okIconContainer").css({"visibility":"visible"});
                        $(".multipleContent").eq(0).find(".serviceTitle").val(arr[0].name);
                        $(".multipleContent").eq(i).attr("data-id", arr[i].id);
                        $(".multipleContent").eq(i).attr("data-count", i);
                        $(".multipleContent").eq(i).find(".serviceTitle").val(arr[i].name);
                        clearTimeout(time);
                    }, 800);
                }

                homeServicesUrl = "updateServices";
                dataInsert = false;
            }
        }).error(function (err, status, error) {
            homeServicesUrl = "saveServices";
            dataInsert = true;
        });


        // let formServices = document.getElementById("homeServicesForm");
        let servicesForm = new FormData();
        let servicesArr = [];
        let textEmpty;
        for (var key in servicesForm.keys()) {
            servicesForm.delete(key);
        }
        $('body').delegate(".serviceTitle", "input", function () {
            $(this).css({"border": ""});
            if ($(this).closest(".multipleContent").attr("data-id") !== undefined) {
                homeServicesUrl = "updateServices";
                dataInsert = false;
            }
            textEmpty = false;
        });

        // Remove Service
        $('body').delegate(".multiply .trashIcon", "click touchstart", function () {
            let dataId = $(this).closest(".multipleContent").attr("data-id");
            $(this).closest(".multipleContent").not(".multipleContent[data-elem='first']").detach();
            // $(this).closest(".form-group").children(".serviceTitle").val('');
            // $(this).closest(".serviceImage").val('');

            if (dataId != undefined && dataId.length > 0) {
                $.ajax({
                    url: "deletePost",
                    method: "POST",
                    data: {"_token": token, "type": "services", "delId": dataId},
                    cache: false,
                    success: function (data) {
                        // console.log(data);

                        $(".content-box.notice").addClass("show");
                        $(".content-box.notice.show").css({"border-color":"orange", "color":"orange"});
                        $(".content-box.notice").text("{{ trans("settings.delete") }}");
                        setTimeout(function() {
                            $(".content-box.notice").removeClass("show");
                        }, 3000);
                    }
                }).error(function (err, status, error) {

                });

                homeServicesUrl = "updateServices";
                dataInsert = false;
            } else {
                homeServicesUrl = "saveServices";
                dataInsert = true;
            }

            // console.log($(this).closest(".multipleContent").attr("data-elem"));

            // if($(".multipleContent").attr("data-count") == $(this).closest(".multipleContent").attr("data-count")) {
            //     servicesForm.delete("text")
            // }

            for (var key in servicesForm.keys()) {
                servicesForm.delete(key);
            }
        });
        $(document).on("click touchstart", ".multiply .multiplyIcon", function () {
            $("#homeServicesForm").prepend("<div class='multipleContent'>" + $("#homeServicesForm .multipleContent").html() + "</div>");

            $(".multipleContent").each(function (count, item) {
                let val = $(".multipleContent").eq(count).find(".serviceImage").val();
                $(".multipleContent").eq(count).find(".serviceImage").trigger('change');
                if(val == '' || val == undefined) {
                    $(".multipleContent").eq(count).find(".okIconContainer").css({"visibility": "hidden"});
                }
            });
            homeServicesUrl = "saveServices";
            dataInsert = true;
        });

        $(".multipleContent")

        $('body').delegate("#homeServicesForm .multipleContent .serviceImage", "change", function () {
            if ($(this)[0].files[0] && typeof $(this)[0].files[0].name !== undefined) {
                $(this).closest(".multipleContent").find(".okIconContainer").css({"visibility": "visible"});
            } else {
                $(this).closest(".multipleContent").find(".okIconContainer").css({"visibility": "hidden"});
            }
        });

        $("#homeServicesForm button[type='submit']").on("click touchstart", function (evt) {
            evt.preventDefault();
            evt.stopPropagation();
            servicesForm.set("_token", token);
            let arrOfIds = [];
            $(".multipleContent").each(function (count, item) {
                let img = $(".multipleContent").eq(count).find(".serviceImage")[0].files[0];
                let txt = $(".multipleContent").eq(count).find(".serviceTitle");

                if (img != undefined && img) {
                    servicesForm.set("img" + count, $(".multipleContent").eq(count).find(".serviceImage")[0].files[0]);

                    // servicesForm.append("text"+count, $(".multipleContent").eq(count).find(".serviceTitle").val());
                } else {
                    // servicesForm.delete("img" + count);
                }
                if (txt.val() != "" && txt) {
                    servicesForm.set("text" + count, $(".multipleContent").eq(count).find(".serviceTitle").val());
                    $(".multipleContent").eq(count).find(".serviceTitle").css({"border": ""});
                } else {
                    $(".content-box.notice").addClass("show");
                    $(".content-box.notice.show").css({"border-color":"red", "color":"red"});
                    $(".content-box.notice").text("{{ trans("settings.error_message") }}");
                    setTimeout(function() {
                        $(".content-box.notice").removeClass("show");
                    }, 3000);
                    textEmpty = true;
                    $(".multipleContent").eq(count).find(".serviceTitle").css({"border": "1px solid red"});
                    servicesForm.delete("text" + count);
                }

                if (homeServicesUrl == "updateServices") {
                    arrOfIds.push($(".multipleContent").eq(count).attr("data-id"));
                } else {
                    arrOfIds.length = 0;
                }
            });

            servicesForm.set("type", "services");
            servicesForm.set("page", pageName);
            servicesForm.set("dataInsert", dataInsert);
            servicesForm.set("ids", JSON.stringify(arrOfIds));

            if (textEmpty)
                return false;
            $.ajax({
                url: homeServicesUrl,
                method: "POST",
                data: servicesForm,
                cache: false,
                processData: false,
                contentType: false,
                success: function (data) {
                    $(".content-box.notice").addClass("show");
                    $(".content-box.notice.show").css({"border-color":"green", "color":"green"});
                    $(".content-box.notice").text("{{ trans("settings.success_save") }}");
                    setTimeout(function() {
                        $(".content-box.notice").removeClass("show");
                    }, 3000);
                    if (data.success) {
                        let oldArr = data.serviceIds;
                        data.serviceIds.reverse();
                        for (let i = 0; i < data.serviceIds.length; i++) {
                            $(".multipleContent").eq(i).attr("data-id", data.serviceIds.id);
                        }
                    }
                    window.location.reload();
                    for (var key in servicesForm.keys()) {
                        servicesForm.delete(key);
                    }
                }
            }).error(function (err, status, error) {
                console.log(error, status);
                for (var key in servicesForm.keys()) {
                    servicesForm.delete(key);
                }
            });

        });


        // Section Our Partners


        let homePartnersForm = new FormData();

        setTimeout(function () {
            $.ajax({
                url: "checkList",
                method: "POST",
                data: {"_token": token, "type": "homePartnersImage"},
                cache: false,
                success: function (data) {
                    for (let itr = 0; itr < data.length; itr++) {
                        let datas = data[itr];
                        let elems = "<div class='image'><span class='delete' data-id=" + datas.id + "><svg viewBox='0 0 1792 1792' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path d='M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z'/></svg>" +
                            "</span><img src=" + datas.image + "></div>";
                        $(".partnersSection .imgsPreview").prepend(elems);
                    }
                }
            }).error(function (err, status, error) {
                console.log(error);
            });
        })

        $("body").delegate(".homeMainSection .partnersSection .image .delete", "click touchstart", function () {
            let that = $(this);
            $.ajax({
                url: "deletePost",
                method: "POST",
                data: {"_token": token, "delId": $(this).attr("data-id")},
                cache: false,
                success: function (result) {
                    that.parent().remove();
                    console.log("Deleted");
                }
            }).error(function (err, status, error) {

            });

        });


        $(".homeMainSection .partnersSection #partnersImg").on("change", function (evt) {
            evt.stopPropagation();
            let here = $(this);
            homePartnersForm.append("_token", token);
            $.each($(this).context.files, function (i, file) {
                homePartnersForm.append("images" + i, file);
            });
            homePartnersForm.append("type", "homePartnersImage");
            homePartnersForm.append("page", pageName);

            $.ajax({
                url: "saveImages",
                method: "POST",
                data: homePartnersForm,
                cache: false,
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.success == true) {
                        for (let itr = 0; itr < here.context.files.length; itr++) {
                            let partnersData = data.partnersIds[itr];
                            let elems = `<div class='image'><span class='delete' data-id= ${partnersData.id}><svg viewBox='0 0 1792 1792' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path d='M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z'/></svg>` +
                                `</span><img src= ${partnersData.image} ></div>`;
                            $(".partnersSection .imgsPreview").prepend(elems);
                        }
                    }
                }
            }).error(function (err, status, error) {

            });
        });


        // Section Contact
        // (function ($) {
        let resultFinal;
        let contactId;
        let adminContact = $("#contactFormAdmin");
        setTimeout(function () {
            $.ajax({
                url: "checkContacts",
                method: "POST",
                data: {"_token": "{{ csrf_token() }}"},
                cache: false,
                success: function (res) {
                    // console.log(res);
                    resultFinal = true;
                    contactId = res.record[0].id;
                    localStorage.setItem("resultFinal", true);
                    adminContact.find("input[name='phone']").val(res.record[0].phone);
                    adminContact.find("input[name='phone']").attr("data-id", res.record[0].id);
                    adminContact.find("input[name='email']").val(res.record[0].email);
                    adminContact.find("input[name='email']").attr("data-id", res.record[0].id);
                    if (res.record[0].address) {
                        adminContact.find("input[name='address']").val(res.record[0].address);
                        adminContact.find("input[name='address']").attr("data-id", res.record[0].id);
                    } else if (res.record[0].addressEn) {
                        adminContact.find("input[name='addressEn']").val(res.record[0].addressEn);
                        adminContact.find("input[name='addressEn']").attr("data-id", res.record[0].id);
                    } else {
                        adminContact.find("input[name='addressRu']").val(res.record[0].addressRu);
                        adminContact.find("input[name='addressRu']").attr("data-id", res.record[0].id);
                    }
                }
            }).error(function (err, status, error) {
                resultFinal = false;
                localStorage.setItem("resultFinal", false);
            });
        }, 600);

        let contactUrl;
        $("body").on("click touchstart", "#contactFormAdmin button", function (evt) {
            evt.preventDefault();
            evt.stopPropagation();

            if (resultFinal || localStorage.getItem("resultFinal") == true) {
                contactUrl = "updateContactData";
            } else {
                contactUrl = "addContactData";
            }
            let formContact = $("#contactFormAdmin");
            $.ajax({
                url: contactUrl,
                method: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "phoneContact": formContact.find("input[name='phone']").val(),
                    "emailContact": formContact.find("input[name='email']").val(),
                    "addressContact": formContact.find("input[name='address']").val(),
                    "addressEnContact": formContact.find("input[name='addressEn']").val(),
                    "addressRuContact": formContact.find("input[name='addressRu']").val(),
                    "contactId": contactId,
                },
                cache: false,
                success: function (result) {
                    // result.record.phone
                    // console.log(result);
                    $(".content-box.notice").addClass("show");
                    $(".content-box.notice.show").css({"border-color":"green", "color":"green"});
                    $(".content-box.notice").text("{{ trans("settings.success_save") }}");
                    setTimeout(function() {
                        $(".content-box.notice").removeClass("show");
                    }, 7000);

                    let record;
                    if (contactUrl == "updateContactData") {
                        record = result.record[0];
                    } else {
                        record = result.record;
                    }

                    formContact.find("input[name='phone']").val(record.phone);
                    formContact.find("input[name='phone']").attr("data-id", result.record[0].id);
                    formContact.find("input[name='email']").val(record.email);
                    formContact.find("input[name='email']").attr("data-id", result.record[0].id);
                    if (record.address) {
                        formContact.find("input[name='address']").val(record.address);
                        formContact.find("input[name='address']").attr("data-id", result.record[0].id);
                    } else if (record.addressEn) {
                        formContact.find("input[name='addressEn']").val(record.addressEn);
                        formContact.find("input[name='addressEn']").attr("data-id", result.record[0].id);
                    } else {
                        formContact.find("input[name='addressRu']").val(record.addressRu);
                        formContact.find("input[name='addressRu']").attr("data-id", result.record[0].id);
                    }
                }
            }).error(function (err, status, error) {
                console.log(err.responseJSON.allErr.emailContact[0]);
                let errContact = err.responseJSON.contactInsertErr;
                $(".content-box.notice").addClass("show");
                $(".content-box.notice.show").css({"border-color":"red", "color":"red"});
                $(".content-box.notice").text(errContact);
                setTimeout(function() {
                    $(".content-box.notice").removeClass("show");
                }, 7000);

                if(Object.keys(err.responseJSON.allErr).length > 0) {
                    $(".contactPhonePreview").text(typeof err.responseJSON.allErr.phoneContact[0] != undefined ? err.responseJSON.allErr.phoneContact[0] : "" ).css({"color":"purple"});
                    $(".contactEmailPreview").text(typeof err.responseJSON.allErr.emailContact[0] != undefined ? err.responseJSON.allErr.emailContact[0] : "").css({"color":"purple"});
                }

            });

        });
        // })(jQuery);


        // subscribers section

        setTimeout(function() {
            $.ajax({
                url: "getSubscribers",
                method: "POST",
                data: {"_token": token},
                caceh: false,
                success: function(result) {
                    if(result.length == 0)
                        return false;
                    result.forEach(function(item, count) {
                        $(".subscribeSection .subscribers").append('<span class="mail" data-id="'+ item.id +'">\n' +
                            '<span class="mailText">'+ item.email +'</span> \n'+
                            '                        <span class="delete">\n' +
                            '                            <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"/></svg>\n' +
                            '                        </span>\n' +
                            '                    </span>');
                    });
                    // console.log(result);
                },
            }).error(function(err, status, error) {

            });

        });

        $("body").delegate(".subscribeSection .subscribers .delete", "click touchstart", function(){
            // $(this).closest('.mail').remove();
            let that = $(this);
            $.ajax({
                url: "deletable",
                method: "DELETE",
                data: {"_token": token, "deletable": $(this).closest(".mail").data("id") },
                cache: false,
                success: function(result) {
                    that.closest(".mail").remove();
                }
            }).error(function(err, status,error){

            });
        });

        let timeoutSubs = setTimeout(function() {

            if($('.subscribeSection .subscribers').find('span.mail').length) {
                $("#homeSubscribersForm button.subscribeSubmit").click(true);
            }else {
                $("#homeSubscribersForm button.subscribeSubmit").click(false);
            }

            clearTimeout(timeoutSubs);
        },2020);

        //
        // if($(".subscribeSection .subscribers .mail").length == 0) {
        //     $("#homeSubscribersForm button.subscribeSubmit").click(false);
        // }else {
        //     $("#homeSubscribersForm button.subscribeSubmit").click(true);
        // }

        // Logout
        let logoutUri = "{{ route('logout', ['language' => app()->getLocale()])  }}";
        $(".adminLogoutBtn").on("click touchstart", function (e) {
            $.ajax({
                url: logoutUri,
                method: "POST",
                data: {"_token": token},
                cache: false,
                success: function (d) {
                    {{--location.href = "{{ route('/', ['language' => app()->getLocale()]) }}"--}}
                    location.reload();
                }
            }).error(function (err) {
                console.log(err);
            })

        });
    })

</script>
</body>
</html>