<footer class="adminfooter">
    <div class="container">

        <div class="copy text-center">
            &copy; Copyright 2020 <a href='#'>New Folder</a>
        </div>

    </div>
</footer>

<script src="{{asset("assets/js/jquery1.1.js")}}"></script>
<script src="{{ asset("assets/bootstrap/js/bootstrap.min.js")}}"></script>
<script src="{{ asset("assets/bootstrap/js/bootstrap-select.js")}}"></script>

<script>

    jQuery.noConflict();
    jQuery(document).ready(function($) {
        let logoutUri = "{{ route('logout', ['language' => app()->getLocale()])  }}";
        $(".adminLogoutBtn").on("click touchstart", function (e) {
            $.ajax({
                url: logoutUri,
                type: "POST",
                data: {"_token": "{{ csrf_token() }}"},
                cache: false,
                success: function (d) {
                    {{--location.href = "{{ route('/', ['language' => app()->getLocale()]) }}"--}}
                    location.reload();
                }
            }).error(function (err) {
                console.log(err);
            })
        });
    });

    let token = "{{ csrf_token() }}";
    let pageName = "{{ Request::segment(3) }}";
    let language = "{{ app()->getLocale() }}";
    let baseUrl = "{{ url('')  }}/"+language;
    let previous = "{{ url()->previous() }}";
    let fullUrl = "{{ url()->full() }}";
    let currentUrl = "{{ url()->current() }}";
    let saveImage = "{{ asset('assets/images/save.svg') }}";
    let successSaveMessage = "{{ trans('settings.success_save') }}";

</script>
{{--<script src="/assets/js/custom.js"></script>--}}
<script src="{{asset("assets/js/custom.js")}}"></script>
</body>
</html>