<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield("pageTitle") </title>
    <!-- Bootstrap -->
    <link href="{{ asset("/assets/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet">

    <!-- styles -->

    <link rel="stylesheet" href="{{ asset("assets/css/owl.carousel.min.css")  }}">
    <link rel="stylesheet" href="{{ asset("assets/css/owl.theme.default.min.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/fontawesome.min.css")  }}">
    <link rel="stylesheet" href="{{ asset("assets/css/all.min.css")  }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{ asset("/assets/css/custom.css")  }}" rel="stylesheet">
    <link href="{{ asset("/assets/css/media/extends.css")  }}" rel="stylesheet">

    <!-- scripts -->
    <script src="{{ asset("assets/js/fontawesome.min.js")  }}"></script>
    <script src="{{ asset("assets/js/jquery-3.2.1.js")  }}"></script>
    <script src="{{ asset("assets/js/owl.carousel.min.js")  }}"></script>
    <script src="{{ asset("assets/js/main.js")  }}"></script>
</head>
<body>
<div class="sction1 {{ Route::currentRouteName() != "home" ? "notHome" : "" }}">
    <header class="{{ Route::currentRouteName() == "home" ? "myContainer" : "" }}">
        {!!  Route::currentRouteName() != "home" ? "<div class='myContainer'>" : "" !!}
        <div class="logo">
            <a href="/"><img src="{{ isset($logo->image) ? asset("$logo->image") : "no image" }}" alt=""></a>
        </div>
{{--            @if(Route::has('login'))--}}
{{--                <blockquote>--}}
{{--                    <a href="/{{ app()->getLocale() }}/admin">Login</a>--}}
{{--                </blockquote>--}}
{{--            @endif--}}
            <div class="searchaAndLang">
                <div class="lang">
                    <a href="{{ route(Route::currentRouteName() , 'hy') }}" class="montserat {{ Request::segment(1) == "hy" ? "hideLang" : "" }}">Հայ </a>
                    <span class="{{ Request::segment(1) == "hy" ? "hideLang" : "" }}">/</span>
                    <a href="{{ route(Route::currentRouteName() , 'ru') }}" class="montserat {{ Request::segment(1) == "ru" ? "hideLang" : "" }}">Ру</a>
                    <span class="{{ Request::segment(1) == "ru" || Request::segment(1) == "en" ? "hideLang" : "" }}">/</span>
                    <a href="{{ route(Route::currentRouteName() , 'en') }}" class="montserat {{ Request::segment(1) == "en" ? "hideLang" : "" }}">En</a>
                </div>
                @if(Request::segment(2) == "careers")
                    <div class="search">
                        <form action="">
                            <input type="text" placeholder="Search" class="inputSearch">
                            <button>
                                <img src="{{ asset("assets/images/search.png") }}" alt="">
                            </button>
                        </form>
                    </div>
                @endif
            </div>
        {!! Route::currentRouteName() != "home" ? "</div>" : "" !!}
        <nav>
            {{-- style="{{ Route::currentRouteName() == "home" ? "color:#0568A7;" : "" }}"--}}
            <ul class="{{ Route::currentRouteName() != "home" ? "myContainer" : "" }}">
                <li><a href="/{{app()->getLocale()}}/about">{{ trans("settings.aboutUs")  }}</a></li>
                <li><a href="/{{app()->getLocale()}}/#contactusForm">{{ trans("settings.contactUs")  }}</a></li>
                <li><a href="/{{app()->getLocale()}}/services">{{ trans("settings.services")  }}</a></li>
                <li><a href="/{{app()->getLocale()}}/portfolio">{{ trans("settings.portfolio") }}</a></li>
                <li><a href="/{{app()->getLocale()}}/careers">{{ trans("settings.career") }}</a></li>
            </ul>
        </nav>
    </header>
    @if(Route::currentRouteName() == "home")
        <div class="firstSectionContent">
            <div class="myContainer">
                <div class="image">
{{--                    img/business-computer-connection-contemporary-450035.png--}}
                    <img src="{{ isset($p1->image) ? asset("$p1->image") : "no img" }}" alt="">
                </div>
                <div class="info">
                    @if(Request::segment(1) == "hy")
                        <h2>{!! isset($p1->name) ? $p1->name : "" !!}</h2>
                        <div class="paragraph">
                            {!! isset($p1->content) ? $p1->content :  "" !!}
                        </div>
                    @elseif(Request::segment(1) == "en")
                        <h2>{!! isset($p1->nameEn) ? $p1->nameEn : "" !!}</h2>
                        <div class="paragraph">
                            {!! isset($p1->contentEn) ? $p1->contentEn :  "" !!}
                        </div>
                    @else
                        <h2>{!! isset($p1->nameRu) ? $p1->nameRu : "" !!}</h2>
                        <div class="paragraph">
                            {!! isset($p1->contentRu) ? $p1->contentRu :  "" !!}
                        </div>
                    @endif
                </div>
                <div class="socials">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
        <div class="yellowBackground"></div>
        <div class="whiteBackgroundWithYellowPoints">
            <img src="{{ asset("assets/images/yellowPoints.png") }}" alt="">
        </div>
    @endif
</div>
{{--<ul>--}}
{{--    <li><a href="{{ route(Route::currentRouteName() , 'en') }}">en</a></li>--}}
{{--    <li><a href="{{ route(Route::currentRouteName() , 'hy') }}">hy</a></li>--}}
{{--</ul>--}}
