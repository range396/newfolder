@extends('layouts.main')

@section('content')
    <main class="aboutPage">
        <img src="{{ asset("assets/images/yellowPoints.png") }}" alt="">
        <div class="myContainer">

            @if(Request::segment(1) == "hy")
                <h2>{!!isset($aboutPost1->name) ? $aboutPost1->name : ''!!}</h2>
                <section class='paragraphAbout'>
                    {!!isset($aboutPost1->content) ? $aboutPost1->content : ''!!}
                </section>
            @elseif(Request::segment(1) == "En")
                <h2>{!!isset($aboutPost1->nameEn) ? $aboutPost1->nameEn : ''!!}</h2>
                <section class='paragraphAbout'>
                    {!! isset($aboutPost1->contentEn) ? $aboutPost1->contentEn : '' !!}
                </section>
            @else
                <h2>{!!isset($aboutPost1->nameRu) ? $aboutPost1->nameRu : ''!!} </h2>
                <section class='paragraphAbout'>
                    {!!isset($aboutPost1->contentRu) ? $aboutPost1->contentRu : ''!!}
                </section>
            @endif
{{--            <h2>About Us</h2>--}}
{{--            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
        </div>
    </main>
@stop


@section("pageTitle")
    {{ trans("settings.aboutUs") }}
@stop