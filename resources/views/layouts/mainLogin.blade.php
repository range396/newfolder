<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Login</title>

    <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="/assets/css/styles.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
    <style>
        /*.loginSubmit {*/
        /*    border:0;*/
        /*    outline:0;*/
        /*}*/
    </style>
</head>
<body>

<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Logo -->
                <div class="logo">
                    <h1><a href="/">NewFolder Admin Login</a></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-content container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-wrapper">
                <div class="box">
                    <div class="content-wrap">
                        <h6>Sign In</h6>
                        <form method="POST" class="loginForm" action="javascript:void(0);">
                            @csrf
                            <input class="form-control" type="text" placeholder="E-mail address" name="email">
                            <span class="invalid-feedback email" role="alert">
                                <strong></strong>
                            </span>
                            <input class="form-control" type="password" placeholder="Password" name="password">
                            <span class="invalid-feedback password" role="alert">
                                <strong></strong>
                            </span>
                            <div class="action">
                                <button type="submit" class="loginSubmit btn btn-primary signup">
                                    Login
{{--                                    <a class="btn btn-primary signup" href="javascript: void(0);"></a>--}}
                                </button>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script src="/assets/js/jquery1.1.js"></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/js/custom.js"></script>
<script>
    // let url = window.location.origin;
    // let path = url + "/login";
    jQuery.noConflict();
    jQuery(document).ready(function($) {
        var typeredirect = true;
        let auth = 0;
        @auth()
            auth = 1;
        @endauth
        var lng = '/{{app()->getLocale()}}';
        var redirect = lng + '/admin/panel';
        let login = "{{ route('login', ['language' => app()->getLocale() ] ) }}";

        $('.loginForm').on('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();

            $.post(login, $(this).serialize(), function (d) {
                location.href = redirect;
            }).fail(function (err) {
                if(err.status == 419) {
                    error = "invald data";
                    location.reload();
                }
                if(err.status == 422) {
                    // console.log(err);
                    if(err.responseJSON.errors.email != null) {
                        $(".invalid-feedback.email").children('strong').text(err.responseJSON.errors.email);
                    }else {
                        $(".invalid-feedback.email").children('strong').text("");
                    }
                    if(err.responseJSON.errors.password != null) {
                        $(".invalid-feedback.password").children('strong').text(err.responseJSON.errors.password);
                    }else {
                        $(".invalid-feedback.password").children('strong').text("");
                    }
                }

                if(err.status == 500) {
                    console.log(err);
                }

            })
        });
    })

</script>
</body>
</html>