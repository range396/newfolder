<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h2>Hello Subscriber,</h2>
{{--@dd($data)--}}

{{ $data['title'] }} <br>
Here are the details:
<div style="color:#f00;">
    <b>Name:</b> {{ $data['title'] }} <br>
    <b color="#f00">Subject:</b> {{ $data['subject']}} <br>
    <b color="#f00">Message:</b> {{ $data['message'] }}
{{--    Requested Ip : {{ $data['ip'] }}--}}
</div>

</body>
</html>