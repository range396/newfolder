@include('includes.admin.header')
<div class="header">
    <div class="container-fluid wrapblock">
        <div class="row">
            <div class="col-md-3">
                <!-- Logo -->
                <div class="logo">
                    <h1><a href="#">NewFolder Admin Dashboard</a></h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name  }}<b
                                            class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li><a class="adminLogoutBtn" href="javascript: void(0);">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-2">
                <div class="navbar langNav navbar-immer" role="banner">
                    <nav class="navbar-collapse" role="navigation">
                        <ul class="nav navbar-nav nav-list">
                            <li>
                                <a href="{{ route(Route::currentRouteName() , 'hy') }}" class="@if(Request::segment(1) == "hy") active @endif">Հայ</a>
                                <span>/</span>
                                <a href="{{ route(Route::currentRouteName() , 'en') }}" class="@if(Request::segment(1) == "en") active @endif">En</a>
                                <span>/</span>
                                <a href="{{ route(Route::currentRouteName() , 'ru') }}" class="@if(Request::segment(1) == "ru") active @endif">Ру</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="row">
        @include('includes.admin.sidebar')
        @if(Request::segment(3) == "panel")
            @include('admin.mainContent')
        @else
            @yield('admContent')
        @endif
    </div>
</div>
@if(Request::segment(3) == 'panel' || Request::segment(3) == 'portfolio')
    @include('includes.admin.footer')
@elseif(Request::segment(3) == 'home')
    @include('includes.admin.homeFooter')
@else
    @include('includes.admin.footerEditors')
@endif