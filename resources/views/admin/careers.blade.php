@extends('admin.dashboard')

@section('admContent')
    <div class="col-md-8 careerPage">
        <h1>Careers Page</h1>
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">{{ trans("settings.createCareer") }}</div>
                <svg class="bi bi-plus-circle multiplyIcon" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 01.5.5v4a.5.5 0 01-.5.5H4a.5.5 0 010-1h3.5V4a.5.5 0 01.5-.5z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 01.5-.5h4a.5.5 0 010 1H8.5V12a.5.5 0 01-1 0V8z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M8 15A7 7 0 108 1a7 7 0 000 14zm0 1A8 8 0 108 0a8 8 0 000 16z" clip-rule="evenodd"></path>
                </svg>
            </div>
        </div>
{{--        <div class="content-box-large">--}}
{{--            <div class="panel-heading">--}}
{{--                <div class="panel-title"></div>--}}
{{--            </div>--}}
{{--            <div class="panel-body">--}}
{{--                <section class="careerHeading">--}}
{{--                    <div class="form-group">--}}
{{--                        <span>Title</span>--}}
{{--                        <input type="text" class="form-control">--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <span>Date and Time</span>--}}
{{--                        <div class='input-group date' id='datetimepicker1'>--}}
{{--                            <input type='text' class="form-control" />--}}
{{--                            <span class="input-group-addon">--}}
{{--                                <span class="glyphicon glyphicon-calendar"></span>--}}
{{--                            </span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <span>Location</span>--}}
{{--                        <input type="text" class="form-control">--}}
{{--                    </div>--}}
{{--                    <div class="form-group passGroup">--}}
{{--                        <span>Content</span>--}}
{{--                        @if(Request::segment(1) == "hy")--}}
{{--                            <textarea class="careerEditor1_hy"></textarea>--}}
{{--                        @elseif(Request::segment(1) == "en")--}}
{{--                            <textarea class="careerEditor1_en"></textarea>--}}
{{--                        @else--}}
{{--                            <textarea class="careerEditor1_ru"></textarea>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                    <div class="form-group passGroup submitBtn">--}}
{{--                        <button class="btn btn-primary">Save</button>--}}
{{--                    </div>--}}
{{--                </section>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
@stop
@section("admTitle")
    {{ trans("settings.career") }}
@stop