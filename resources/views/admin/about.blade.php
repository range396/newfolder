@extends('admin.dashboard')

@section('admContent')
    <div class="col-md-8">
        <div class="content-box-large pageAbout">
            <div class="panel-heading">
                <div class="panel-title">
                    <div class="form-group">
                        <span align="right"><span>{{ trans('settings.title') }}</span>
                            @if(Request::segment(1) == "hy")
                                <input type="text" class="aboutMainPostTitle form-control" data-lang="hy">
                            @elseif(Request::segment(1) == "en")
                                <input type="text" class="aboutMainPostTitle form-control" data-lang="en">
                            @else
                                <input type="text" class="aboutMainPostTitle form-control" data-lang="ru">
                            @endif
                        </span>
                    </div>
                </div>
                <div class="panel-title">
                    <div class="deleteButtonblock">
                        <span class="deleteHeader deleteContainer" data-id="">
                            <a href="javascript:void(0);">{{ trans('settings.delete') }}</a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form action="javascript: void(0);">
                    @if(Request::segment(1) == "hy")
                        <textarea id="aboutEditor1_hy"></textarea>
                    @elseif(Request::segment(1) == "en")
                        <textarea id="aboutEditor1_en"></textarea>
                    @else
                        <textarea id="aboutEditor1_ru"></textarea>
                    @endif
                </form>
            </div>
        </div>
        <div class="content-box notice">

        </div>
    </div>
@stop
@section("admTitle")
    {{ trans("settings.aboutUs") }}
@stop