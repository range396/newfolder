@extends('layouts.main')

@section('content')
    <main>
        <div class="section2">
            <div class="myContainer">
                <div class="text">
                    @if(Request::segment(1) == "hy")
                        <h3>{!! isset($p2->name) ? $p2->name: "" !!}</h3>
                        <div class="paragraph">
                            {!! isset($p2->content) ? $p2->content : "" !!}
                        </div>
                    @elseif(Request::segment(1) == "en")
                        <h3>{!! isset($p2->nameEn) ? $p2->nameEn : ""!!}</h3>
                        <div class="paragraph">
                            {!! isset($p2->contentEn) ? $p2->contentEn : "" !!}
                        </div>
                    @else
                        <h3>{!! isset($p2->nameRu) ? $p2->nameRu : "" !!}</h3>
                        <div class="paragraph">
                            {!! isset($p2->contentRu) ? $p2->contentRu : "" !!}
                        </div>
                    @endif
                    <div class="more">
                        <a href="about">{{ trans("settings.readMore") }}</a>
                    </div>
                    <img src="{{ asset('assets/images/whitebullets.png') }}" alt="">
                </div>
                <div class="image">
                    <img src="{{ isset($p2->image) ? asset("$p2->image") : "" }}" alt="">
                </div>
            </div>
            <div class="blueBackground"></div>
        </div>
        <div class="section3">
            <div class="ourServices myContainer">
                <h2>{{ trans("settings.services") }}</h2>
                <div class="items">
                    @if(isset($services))
                        @foreach($services as $service)
                            <div class="item">
                                <div class="image">
                                    {{--                                    img/services/crm.png--}}
                                    <img src="{{ isset($service->image) ? asset("$service->image") : ""  }}" alt="">
                                    <div class="hoverOrder">
                                        <span>order</span>
                                    </div>
                                </div>
                                <div class="serviceName">
                                    <p><span>{{ isset($service->name) ?  $service->name : "No name" }}</span></p>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
            <div class="ourPartners myContainer">
                <h2>{{ trans("settings.partners") }}</h2>
                <div class="owlarousel">
                    <div class="owl-carousel owl-theme  ">
                        @if(isset($partners))
                            @foreach($partners as $partner)
                                <div class="item">
                                    <div class="owlItemContent">
                                        {{--                                        img/partners/nfl3.png--}}
                                        <img src="{{ isset($partner->image) ? asset("$partner->image") : "" }}" alt="">
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        {{--                        <div class="item">--}}
                        {{--                            <div class="owlItemContent">--}}
                        {{--                                <img src="img/partners/nfl3.png" alt="">--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="item">--}}
                        {{--                            <div class="owlItemContent">--}}
                        {{--                                <img src="img/partners/logo.png" alt="">--}}
                        {{--                            </div>--}}

                        {{--                        </div>--}}
                        {{--                        <div class="item">--}}
                        {{--                            <div class="owlItemContent">--}}
                        {{--                                <img src="img/partners/logo without gradient-01.png" alt="">--}}
                        {{--                            </div>--}}

                        {{--                        </div>--}}
                        {{--                        <div class="item">--}}
                        {{--                            <div class="owlItemContent">--}}
                        {{--                                <img src="img/partners/fingerlogo-01.png" alt="">--}}
                        {{--                            </div>--}}

                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="myContainer contactUs" id="contactusForm">
                <div class="infoPart">
                    <h2>{{ trans("settings.contactUs") }}</h2>
                    <div class="infoItems">
                        <div class="item">
                            <p class="name">{{ trans("settings.callUs") }}</p>
                            <div class="imageAndContent">
                                <p class="image">
                                    <i class="fa fa-phone"></i>
                                </p>
                                <a href="javascript:void(0);">{{ isset($contactPhone) ? $contactPhone : "" }}</a> {{-- +374 (44) 355 488 --}}
                            </div>
                        </div>
                        <div class="item">
                            <p class="name">{{ trans("settings.findUs") }}</p>
                            <div class="imageAndContent">
                                <p class="image">
                                    <i class="fas fa-map-marker-alt"></i>
                                </p>
                                <a href="javascript:void(0);">
                                    @if(Request::segment(1) == "hy")
                                        {!! isset($contactAddress) ? $contactAddress : "" !!}
                                    @elseif(Request::segment(1) == "en")
                                        {!! isset($contactAddressEn) ? $contactAddressEn : "" !!}
                                    @else
                                        {!!  isset($contactAddressRu) ? $contactAddressRu : "" !!}
                                    @endif

                                </a> {{--info@newfolder.am--}}
                            </div>
                        </div>
                        <div class="item">
                            <p class="name">{{ trans("settings.sendEmail") }}</p>
                            <div class="imageAndContent">
                                <p class="image">
                                    <i class="fas fa-envelope"></i>
                                </p>
                                <a href="javascript:void(0);">
                                    {{ isset($contactEmail) ? $contactEmail : "" }}
                                </a> {{-- Hakob Hakobyan 3 --}}
                            </div>
                        </div>
                        <div class="item">
                            <p class="name">{{ trans("settings.subscribe") }}</p>
                            <div class="imageAndContent">
                                <form action="/{{app()->getLocale()}}/subscribe" method="POST">
                                    @csrf
                                    <input type="text" name="subscribe" value="{{ old('subscribe') }}" placeholder="E-mail">
                                    <button type="submit">
                                        <i class="fab fa-telegram-plane"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <img src="{{ asset("assets/images/whitebullets.png") }}" alt="">
                </div>
                <div class="formToContact">
                    <div class="content-box notice">
                        
                    </div>
                    
                    <form action="javscript:void(0);" method="" id="homeMailForm">
                        <span class="nameValid"></span>                        
                        <input type="text" placeholder="{{trans("settings.fullName")}}" value="{{ old('contacterName') }}" name="contacterName">
                        <span class="emailValid"></span>
                        <input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="{{trans("settings.email")}}" value="{{ old('email') }}" name="email">
                        <span class="messageValid"></span>    
                        <textarea placeholder="{{ trans("settings.message") }}" value="{{ old('message') }}" name="message" id="" cols="30" rows="10"></textarea>
                        <input type="submit" value="{{ trans("settings.save") }} ">
                        
                    </form>
                </div>
            </div>
        </div>
    </main>
    {{--    <div class="contact">--}}
    {{--        @error('contacterName')--}}
    {{--            <p color="#f00">Contacter Name error</p>--}}
    {{--        @enderror--}}
    {{--        <form action="{{Request::segment(1)}}/sendMail" method="POST">--}}
    {{--            @csrf--}}

    {{--            @if($errors->any())--}}
    {{--                {!! implode('', $errors->all('<div>:message</div>')) !!}--}}
    {{--            @endif--}}

    {{--            @if(Session::has('success'))--}}
    {{--                <p color="#0f0">{!! Session::get('success') !!}</p>--}}
    {{--            @endif--}}
    {{--            @if(Session::has('contactError'))--}}
    {{--                <p color="#f00">{!! Session::get('contactError')  !!}</p>--}}
    {{--            @endif--}}
    {{--            @error('contacterName')--}}
    {{--            <h1>Name@ chka</h1>--}}
    {{--            @endError--}}
    {{--            <div class="form-group">--}}
    {{--                <input type="text" placeholder="Name" name="contacterName" value="{{ old('contacterName') }}" class="form-control">--}}
    {{--            </div>--}}
    {{--            <div class="form-group">--}}
    {{--                <input type="text" placeholder="Email" name="email" value="{{ old('email') }}" class="form-control">--}}
    {{--            </div>--}}
    {{--            <div class="form-group">--}}
    {{--                <textarea placeholder="Message" name="message" value="{{ old('message') }}" class="form-control"></textarea>--}}
    {{--            </div>--}}
    {{--            <button type="submit" class="btn btn-primary">Submit</button>--}}
    {{--            @if(Session::has('technicalIssue'))--}}
    {{--                {{Session::get('technicalIssue')}}--}}
    {{--            @endif--}}
    {{--        </form>--}}
    {{--    </div>--}}


@endsection

@section("pageTitle")
    {{ trans("settings.homeTitle")  }}
@stop