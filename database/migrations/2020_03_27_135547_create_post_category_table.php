<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postCategory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('postId')->index();
            $table->unsignedBigInteger('catId')->index();
            $table->foreign('postId')->references('id')->on('posts')->onDelete('cascade');
            $table->foreign('catId')->references('id')->on('categories')->onDelete('cascade');
//            $table->primary(array('postId', 'catId'));
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postCategory');
    }
}
