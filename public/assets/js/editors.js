jQuery.noConflict();
jQuery(document).ready(function($) {
	$(function() {
		// Bootstrap
		$('#bootstrap-editor').wysihtml5();

		// Ckeditor standard
		$( 'textarea#ckeditor_standard' ).ckeditor({width:'98%', height: '150px', toolbar: [
				{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
				[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
				{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
			]});

		$( 'textarea#ckeditor_full' ).ckeditor({width:'98%', height: '150px'});
	});
})

// Tiny MCE
// tinymce.init({
//     selector: "#tinymce_basic",
//     plugins: [
//         "advlist autolink lists link image charmap print preview anchor",
//         "searchreplace visualblocks code fullscreen",
//         "insertdatetime media table contextmenu paste"
//     ],
//     toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
// });

// Tiny MCE
// tinymce.init({
//     selector: "#tinymce_full",
//     plugins: [
//         "advlist autolink lists link image charmap print preview hr anchor pagebreak",
//         "searchreplace wordcount visualblocks visualchars code fullscreen",
//         "insertdatetime media nonbreaking save table contextmenu directionality",
//         "emoticons template paste textcolor"
//     ],
//     toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
//     toolbar2: "print preview media | forecolor backcolor emoticons",
//     image_advtab: true,
//     templates: [
//         {title: 'Test template 1', content: 'Test 1'},
//         {title: 'Test template 2', content: 'Test 2'}
//     ]
// });



//
// tinyMCE.init({
// 	selector: "#tinymce_full",
// 	inline: false,
// 	setup: function (editor) {
// 		console.log("data Loaded");
// 		// console.log(editor)
// 	},
// 	plugins: [
// 		"advlist autolink lists link image charmap print preview hr anchor pagebreak",
// 		"searchreplace wordcount visualblocks visualchars code fullscreen",
// 		"insertdatetime media nonbreaking save table contextmenu directionality",
// 		"emoticons template paste textcolor fullpage",
// 	],
// 	toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
// 	toolbar2: "print preview media | forecolor backcolor emoticons | save",
// 	image_advtab: true,
// 	language: '{{ app()->getLocale() }}',
// 	fullpage_hide_in_source_view: true,
// 	submit_patch: true,
// 	invalid_elements: 'script',
// 	protect: [
// 		/\<\/?(if|endif)\>/g,  // Protect <if> & </endif>
// 		/\<xsl\:[^>]+\>/g,  // Protect <xsl:...>
// 		/<\?php.*?\?>/g,  // Protect php code
// 		/<\?script.*?\?>/g,  // Protect js code
// 	],
// 	// schema:"html",
// 	verify_html: false,
// 	// allow_script_urls: true,
// 	// apply_source_formatting: false,
// 	element_format : 'html',
// 	templates: [
// 		{title: 'Test template 1', content: 'Test 1'},
// 		{title: 'Test template 2', content: 'Test 2'}
// 	],
// 	save_onsavecallback: function (e) { //https://www.tiny.cloud/docs/plugins/save/
//
// 		// let data = String(tinymce.get('tinymce_full').getContent({format: 'xml'}));
// 		let currentEditor = tinyMCE.get('tinymce_full');
// 		let data = currentEditor.getContent();
// 		currentEditor.setProgressState(true);
// 		setTimeout(function(){
// 			currentEditor.setProgressState(false);
// 		},2000);
//
// 		// currentEditor.setProgressState(true);
// 		$.ajax({
// 			url: '/{{ app()->getLocale() }}/admin/pSave',
// 			method: "POST",
// 			data: {"_token": "{{ csrf_token() }}", "contentText": data},
// 			cache: false,
// 			success: function (res) {
// 				console.log('Saved');
// 				console.log(res);
// 			}
// 		}).error(function (err, error) {
//
// 		});
//
// 	},
// });
//
//
// $('select option').on("change", function () {
// 	tinyMCE.execCommand("mceRepaint");
// });