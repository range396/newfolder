<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $fillable = ['name', "content", "contentRu", "contentEn", "date", "location", "page", "created_at", "updated_at"];
    protected $table = "careers";
    public $timestamps = true;

    public function category() {
        return $this->belongsToMany('App\Category', 'relation', 'careerId', 'categId')->withTimestamps();
    }


}
