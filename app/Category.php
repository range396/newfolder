<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    //
    protected $fillable = ['name', 'nameRu', 'nameEn','categoryType','created_at','updated_at'];
//    protected $guarded = [];
    protected $table = 'categories';
    public $timestamps = true;

    public function posts() {
        return $this->belongsToMany('App\Post', 'postcategory','catId', 'postId')->withTimestamps();
//        return $this->belongsToMany('App\Post')->using('App\PostCategory');
    }

    public function career() {
        return $this->belongsToMany('App\Career', 'relation', 'catId', 'careerId')->withTimestamps();
    }

}