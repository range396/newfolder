<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $fillable = ['ordercatId','fullname', 'website', 'phone', 'email', 'message', 'ip', 'created_at', 'updated_at'];
    protected $table ='orders';
    public $timestamps = true;

    public static function orderCat() {
        return parent::hasOne('App\Catorder');
    }

    public static function getOrderBy($id = null, $email = null) {
        if(isset($id)) {
            return parent::find($id)->first();
        }
        if(isset($email)) {
          return parent::where('email', $email)->get();
        }
    }
}
