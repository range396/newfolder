<?php

namespace App\Mail;

use App\Mailing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Mailing::create($this->data);

        return $this->from('asdftest12345zxx@gmail.com')
            ->view('mails.newmail')
//            ->with([
//                'param1' => 'asdasd',
//                'param2' => '',
//            ])
//            ->attach('filename.jpg, png',[
//                'as' => 'filename.jpg'
//            ])
;

    }
}
