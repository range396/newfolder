<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mailing;
use Illuminate\Mail\Mailable;
use File;
use Illuminate\Queue\SerializesModels;

class Career extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $store;
    public $file;

    public function __construct($data, $store)
    {
        $this->data = $data;
        $this->store = $store;
        $this->file = base64_encode(File::get($data['cv'])); //Storage::disk('local')->get('marquee.json');
    }

    public function build()
    {
        Mailing::create($this->store);
        return $this->from('asdftest12345zxx@gmail.com', $this->data["MailTitle"] )
            ->subject($this->data['subject'])
            ->view('mails.career')->attachData(base64_decode($this->file), $this->data['cv']->getClientOriginalName(), [
                'mime' => $this->data['cv']->getMimeType()
            ]);
    }
}
