<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post as P;
use App\Contact as C;
use App\Category as Cat;
use App\Career;
use App\Http\Controllers\Auth\LoginController as Login;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

class PageController extends Controller
{
    public $logo;
    public $services;
    public $partners;
    public $p1;
    public $p2;
    public $firstContact;

    public function __construct() {
        $homePosts = P::allPosts("home");
        $this->firstContact = C::getFirst();
        $p1 = null;
        $p2 = null;
        $logo = null;
        $servicesArr = array();
        $partnersArr = array();
        foreach($homePosts as $post) {
            if($post->type == "cke_1") {
                $p1 = $post;
            }
            if($post->type == "cke_2") {
                $p2 = $post;
            }
            if($post->type == "mainLogo") {
                $logo = $post;
            }
            if($post->type == "services") {
                $servicesArr[] = $post;
            }
            if($post->type == "homePartnersImage") {
                $partnersArr[] = $post;
            }
        }
        $this->services = array_reverse($servicesArr);
        $this->partners = $partnersArr;
        $this->p1 = $p1 !== null ? $p1 : "";
        $this->p2 = $p2 !== null ? $p2 : "";
        $this->logo = $logo !== null ? $logo : "";

    }

    public function home() {
        $p1 = $this->p1;
        $p2 = $this->p2;
        $services = $this->services;
        $partners = $this->partners;
        $logo = $this->logo;
        $contactPhone = isset($this->firstContact->phone) ? $this->firstContact->phone : "";
        $contactEmail = isset($this->firstContact->email) ? $this->firstContact->email : "";
        $contactAddress = isset($this->firstContact->address) ? $this->firstContact->address : "";
        $contactAddressRu = isset($this->firstContact->addressRu) ? $this->firstContact->addressRu : "";
        $contactAddressEn = isset($this->firstContact->addressEn) ? $this->firstContact->addressEn : "";
        return view('home', compact("p1","p2","logo", "services","partners","contactPhone","contactEmail",
        "contactAddress", "contactAddressRu", "contactAddressEn"
        ));
    }

    public function about() {
        $aboutPosts = P::allPosts("about");
        $aboutPost1 = null;
        foreach($aboutPosts as $post) {
            if($post->type == "cke_1") {
                $aboutPost1 = $post;
            }
        }
        $logo = $this->logo;
        $aboutPost1 = $aboutPost1 !== null ? $aboutPost1 : "";

        return view('about', compact("logo", "aboutPost1"));
    }

    public function services() {
        $logo = $this->logo;
        $services = P::where('type', 'services')->get();

        return view('services', compact(['logo', 'services']));
    }

    public function paginate($items, $perPage = 4, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function portfolio(Request $request) {
        $logo = $this->logo;
        $categories = Cat::where('categoryType', 'portfolio')->get();
        $posts = P::where('page','portfolio')->orderBy('id','DESC')->paginate(4);
        if($request->ajax()) {
            if(isset($request->filterIds)) {
                $ok = Cat::with("posts")->whereIn('id', json_decode($request->filterIds))->get();
                $postArr = [];
                if($ok) {
                    foreach($ok as $cat) {
                        foreach($cat->posts as $p) {
                            $postArr[] = $p;
                        }
                    }

                    $postByCat = $this->paginate($postArr);
                    return view('includes.portfolioPagination', compact('postByCat'));
                }
            }else {
                return view('includes.portfolioPagination', compact(['posts','categories']));
            }
        }
        return view('portfolio', compact(['logo','posts','categories']));
    }

    public function careers() {
        $logo = $this->logo;
        $careers = Career::select("*")->with('category')->get();
        return view('careers', compact(["logo", "careers"]));
    }

    public function admin(Login $login) {
        return $login->showLoginForm();
    }

//    public function test() {
//        //https://www.sitepoint.com/managing-cronjobs-with-laravel/
//    }
}
