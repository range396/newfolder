<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Mailing;
use Carbon\Carbon;
use Validator;

class OrderController extends Controller
{

    public function show(Request $request) {

    }

    public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) == true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }

    public function storeOrder(Request $request) {
       if($request->ajax()) {
           $valid = Validator::make($request->all(), [
               'serviceFullName' => 'required|min:1',
//               'serviceWebsite' => 'required|regex:/[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/',
               'servicePhone' => 'required',
               'serviceEmail' => 'required|email',
               'serviceCategoryVal' => 'required',
               'serviceText' => 'required|min:1'
           ]);

           if($valid->fails()) {
                return response()->json(['erorrServices' => __("settings.serviceError"), "allErr" => $valid->messages()],422);
           }else {
               $clientIp = $this->getIp();
               $ok = Order::create([
                   "fullname" => $request->serviceFullName,
                   "website" => $request->servicesWebsite,
                   "cat" => $request->serviceCategoryVal,
                   "phone" => $request->servicePhone,
                   "email" => $request->serviceEmail,
                   "message" => $request->serviceText,
                   "ip" => isset($clientIp) ? $clientIp : $request->getClientIp()
               ]);

               if($ok)
                   return response()->json(["success" => $ok, "message" => __("settings.success_message")], 200);
           }
       }

    }

    public function removeApplicant(Request $request) {
        if($request->ajax()) {
            $ok = Order::find($request->deleteId)->delete();
            if($ok)
                return response()->json("deleted", 200);
            return response()->json("fail", 422);
        }
    }

}
