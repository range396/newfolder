<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Validator;
use Exception;
use App\Post;
use Carbon\Carbon;
use Mavinoo\LaravelBatch\Batch;




class UploadController extends Controller
{

    public function __construct() {
//        $this->middleware('auth');
    }

    public function imageListSave(Request $request, Batch $batch) {
        if($request->ajax()) {

            if(Post::select('id')->first() != null) {
                $latestPost = Post::select('id')->orderBy('id', 'DESC')->first()->id;
            }else {
                $latestPost = 0;
            }

            $instance = new Post;
            $columns = [
                'userId',
                'type',
                'page',
                'image',
                'created_at',
                'updated_at'
            ];
            $values = array();
            $arrInLoop = array();
            $all = $request->all();
            $baseUrl = $request->getBaseUrl();
            $reqImgs = $request->only("images");
//            $collection= collect($all)->only('images');
//            $imgArray = $collection->toArray();



            foreach($all as $key => $img) {
                if(strpos($key,"images") !== false) {
                    $hasErr = Validator::make($request->all(), [
                        "$key" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048",
                    ]);
                    if($hasErr->fails()) {
                        return response()->json(['errorUpload' => __('settings.catupload')], 422);
                    }
                    $imageName = "partner$key" . '.' . $img->extension();
                    $arrInLoop["userId"] = Auth::user()->id;
                    $arrInLoop["type"]   = $request->type;
                    $arrInLoop["page"]   = $request->page;
                    $arrInLoop["image"]  = $baseUrl . '/assets/images/upload/' . $imageName;
                    $arrInLoop["created_at"]  = Carbon::now();
                    $arrInLoop["updated_at"]  = Carbon::now();
                    $img->move(public_path('assets/images/upload'), $imageName);
                    $values[] = $arrInLoop;


                }
            }



            $size = 500;
            $result = $batch->insert($instance, $columns, $values, $size);
            if($result) {
                $insertedIds = Post::select(['id', 'name', 'image', 'type'])->where('id', '>', $latestPost)->get();
                return response()->json(['success' => true,'partnersIds' => $insertedIds ], 200);
            }else {
                return response('fail', 422);
            }

        }
    }

    public function logoUpload(Request $request) {
        try {
           $hasErr = Validator::make($request->all(), [
                "imageLogo" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2000000",
           ]);

           if($hasErr->fails()) {
                return back()->with('errorUpload', __('settings.catupload'))->withErrors(['imageErr', __('settings.imageErr')]);
           }

            $imageName = "mainLogo" . '.' . $request->imageLogo->extension();
            $request->imageLogo->move(public_path('assets/images/upload'), $imageName);
//            dd(public_path('assets\images\upload\\') . $imageName);
            $img = $request->getBaseUrl() . '/assets/images/upload/' . $imageName;
            Post::where('type', 'mainLogo')->delete();
            $createPost =  Post::create([
               "userId" => Auth::user()->id,
               "image" => "{$img}",
               "type" => "mainLogo",
               "page" => "home",
               "created_at" => Carbon::now(),
               "updated_at" => Carbon::now(),
            ]);

            if($createPost) {
                return back()->with("success", __('settings.successUpload'))->with('imagePath', $createPost->image);
            }

        } catch(Exception $e){
            dd($e);
        }
    }
}
