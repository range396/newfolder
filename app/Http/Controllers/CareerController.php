<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Career;
use Carbon\Carbon;
use App\Relation;
use Mavinoo\LaravelBatch\Batch;
use App\Http\Controllers\CategoryController;

class CareerController extends Controller
{
    //
    public function store(Request $request, Batch $batch, CategoryController $c) {
        if($request->ajax()) {

//            $instance = new Relation;
            $careerArr = array();
//            $columns = [
//                'careerId',
//                'categId',
//                'created_at',
//                'updated_at'
//            ];
            $values = array();
            $careerArr["name"] = $request->title;

            if($request->lang == "hy") {
                $careerArr["content"] = $request->textContent;
            }else if ($request->lang == "en") {
                $careerArr["contentEn"] = $request->textContent;
            }else {
                $careerArr["contentRu"] = $request->textContent;
            }
            $careerArr["date"] = Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');
            $careerArr["location"] = $request->location;
            $careerArr["page"] = $request->pageName;
            $careerArr["created_at"] = Carbon::now();
            $careerArr["updated_at"] = Carbon::now();

            $ok = Career::create($careerArr);
            $okCat = $c->multipleStoreCategory($request, false);
            $ids = array();
            foreach($okCat['ids'] as $cat) {
                $ids["careerId"] = "$ok->id";
                $ids["categId"] = "$cat->id";
                $ids["created_at"] = Carbon::now();
                $ids["updated_at"] = Carbon::now();
                $values[] = $ids;
            }

            $okRelation = Relation::insert($values);

//            $size = 600;
//            $okRelation = $batch->insert($instance, $columns, $values, $size);
            if($ok && $okCat['ok'] && $okRelation) {
                $data = Career::with('category')->where("id" , $ok->id)->get();
                return response()->json(["fullData" => $data], 200);
            }
            return response()->json("fail", 422);
        }
    }

    public function show(Request $request) {
        if($request->ajax()) {
            $data = Career::with('category')->where("page", $request->type)->get();

            if(isset($data))
                return response()->json(["all" => $data],200);
            return response()->json("fail",422);
        }
    }

    public function edit(Request $request, Batch $batch, CategoryController $c) {
        if($request->ajax()) {
            $career = Career::find($request->careerId);
            $values = array();
            $carArr = array();
            $carArr["name"] = $request->title;
            $carArr["date"] = Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');
            $carArr["location"] = $request->location;

            if($request->lang == "hy") {
                $carArr["content"] = (string) $request->textContent;
            }else if($request->lang == "ru") {
                $carArr["contentRu"] = (string) $request->textContent;
            }else {
                $carArr["contentEn"] = (string) $request->textContent;
            }
         //   dd($carArr);
            $okCarr = $career->update($carArr);

            if(isset($request->newCat) && count(json_decode($request->newCat)) > 0) {
                $okCat = $c->multipleStoreCategory($request, true);
                $ids = array();
                foreach($okCat['ids'] as $cat) {
                    $ids["careerId"] = "$request->careerId";
                    $ids["categId"] = "$cat->id";
                    $ids["created_at"] = Carbon::now();
                    $ids["updated_at"] = Carbon::now();
                    $values[] = $ids;
                }
                $okRelation = Relation::insert($values);
            }

            if($okCarr || $okCat['ok'] && $okRelation) {
                $data = Career::with('category')->where("id" , $request->careerId)->get();
                return response()->json(["fullData" => $data], 200);
            }
            return response()->json("fail", 422);
        }
    }

    public function destroyCat(Request $request, CategoryController $c) {
        if($request->ajax()) {
            $ok = $c->destroy($request);
            if($ok)
                return response()->json("deleted", 200);
            return response()->json("fail", 422);
        }
    }

    public function destroy(Request $request) {
        if($request->ajax()) {
            $id = $request->delId;
            $cats = Career::find($request->delId)->with('category')->get();
            $catIdsArr = array();
            foreach($cats as $c) {
                foreach($c->category as $categ) {
                    $catIdsArr[] = $categ->id;
                }
            }
            $okCat = Category::whereIn('id', $catIdsArr)->delete();
            $ok = Career::find($request->delId)->delete();

            if($ok && $okCat)
                return response()->json("deleted", 200);
            return response()->json("fail", 422);

        }
    }

    public function search(Request $request) {
       if($request->ajax()) {
           $careerName = Career::where("name" , 'like', "%{$request->get('q')}%")->orderBy('created_at', 'desc')->get();
           if($careerName)
               return response()->json(["srch" => $careerName], 200);
           return response()->json(["srch" => $careerName], 422);
       }
    }

}
