<?php

namespace App\Http\Controllers;

use App\Mail\NewMail;
use App\Mail\Subscribe;
use App\Mail\Career as careerMail;
use Illuminate\Http\Request;
use App\Mailing;
use Mail;
use Validator;
use Exception;

class MailController extends Controller
{

    public function __construct() {

//        $this->middleware(['auth','guest']);
//        $this->middleware('guest');
//        $this->middleware('auth');
    }
    //

    public function contactData(Request $request)
    {

       if($request->ajax()) {

            $hasError = Validator::make($request->all(), [
               'contacterName' => 'required|min:1',
               'email' => 'required|email',
               'message' => 'required|min:1'
            ]);

           if ($hasError->fails()) {
               return response()->json(["contactError" => __("settings.error_message"), "allErr" => $hasError->messages()], 422);
           } else {

               $clientIp = $this->getIp();
               Mail::to(['playlivepiano@outlook.com'])->send(new NewMail([
                   "title" => $request->contacterName,
                   "email" => trim($request->email),
                   "subject" => "guestContact",
                   "message" => $request->message,
                   'stored' => false,
                   "subscribe" => false,
                   'ip' => isset($clientIp) ? $clientIp : $request->getClientIp()
               ]));

               return response()->json(["success" => __('settings.success_message')], 200);
           }
       }
        
    }

//    public function test(Request $request) {
//        dd($this->getIp());
//    }

    public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) == true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }

    public function subscription(Request $request) {
        try {

            $errExists = Validator::make($request->all(), [
                'subscribe' => 'required|email',
            ]);

            if ($errExists->fails()) {
                return back()->with("subscribeError", __("settings.error_message"))->withErrors($errExists);
            } else {

                $clientIp = $this->getIp();
                $ok =  Mailing::create([
                    "email" => trim($request->subscribe),
                    "subject" => "subscriber",
                    'stored' => false,
                    "subscribe" => true,
                    "ip" => isset($clientIp) ? $clientIp : $request->getClientIp()
                ]);

              if($ok) {
                  return back()->with("success", __('settings.success_message'));
              }

            }

        } catch (\Exception $e) {
            dd($e);
//            return back()->with("technicalIssue", "We have Technical Issue. Sorry , functionality will be back soon.");
//            abort(404);
        }
    }

    public function getSubscribers(Request $request) {
        return Mailing::where('subscribe', '1')->get();
    }

    public function sendToSubscribers(Request $request) {
        try {

            $subscribers = Mailing::where("subscribe", "1")->get();
            $mails = array();

            foreach($subscribers as $sub) {
                $mails[] = $sub->email;
            }

//            $clientIp = $this->getIp();
            Mail::to($mails)->send(new Subscribe([
                "title" => "NewFolder Answer",
                "subject" => "new Folder",
                "message" => $request->subscriberMessage,
            ]));

            return back()->with("success");

        }catch(Exception $e) {
            return back()->with("fail");
//            dd($e);
        }
    }

    public function deletable(Request $request) {
        $ok = Mailing::find($request->deletable)->delete();
        if($ok)
            return response()->json("deleted", 200);
        return response()->json("fail", 422);
    }


    public function test(Request $request) {
        dd($request->cvFile);
    }

    public function careerMail(Request $request) {
        try {
            $v = Validator::make($request->all(), [
                'careerName'    => 'required|min:1',
                'careerEmail'   => 'required|email',
                'cvFile'        => 'required|mimes:doc,pdf,docx',
                'careerMessage' => 'required|min:1',
            ]);

            if ($v->fails()) {
                return back()->with("mailError", __("settings.error_mail"))->withErrors($v);
            } else {

               $clientIp = $this->getIp();
               Mail::to(['playlivepiano@outlook.com'])->send(new careerMail([
                    "title" => $request->careerName,
                    "email" => $request->careerEmail,
                    "subject" => $request->position,
                    "message" => $request->careerMessage,
                    "MailTitle" => "New folder Position",
                    "link" => $request->cvLink,
                    'stored' => false,
                    "subscribe" => false,
                    "cv" => $request->file('cvFile'),
                    'ip' => isset($clientIp) ? $clientIp : $request->getClientIp()
               ],[
                   "title" => $request->careerName,
                   "email" => $request->careerEmail,
                   "subject" => $request->position,
                   "message" => $request->careerMessage,
                   "link" => $request->cvLink,
                   'stored' => false,
                   "subscribe" => false,
                   'ip' => isset($clientIp) ? $clientIp : $request->getClientIp()
               ]));

               return back()->with("success", __('settings.success_message'));
            }
        }catch(Exception $e) {
            return back()->with("exception", $e);
//           dd($e);
        }
    }

}

/*
 * MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=0eca2c702a9fc8
MAIL_PASSWORD=b699389ee606e4
MAIL_ENCRYPTION=null
 * */
