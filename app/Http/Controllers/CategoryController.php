<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;
use Mavinoo\LaravelBatch\Batch;
use Carbon\Carbon;

class CategoryController extends Controller
{
    //
    public function store(Request $request) {
        if($request->ajax()) {
            $ok = Category::create([
                "name" => $request->name,
                "categoryType" => $request->type
            ]);

            if($ok)
                return response()->json(["currentId" => $ok->id], 200);
            return response()->json("fail", 422);
        }
    }

    public function multipleStoreCategory($req, $new) {
        if(Category::select('id')->first() != null) {
            $latestPost = Category::select('id')->orderBy('id', 'DESC')->first()->id;
        }else {
            $latestPost = 0;
        }
        $catArr = [];
        $finalArr = [];
        $skills = !$new ? json_decode($req->skills) : json_decode($req->newCat);
        foreach($skills as $skill) {
            $catArr["name"] = $skill;
            $catArr["created_at"] = Carbon::now();
            $catArr["updated_at"] = Carbon::now();
            $catArr["categoryType"] = $req->catType;
            $finalArr[] = $catArr;
        }

        $ok = Category::insert($finalArr);
        $catIds = Category::select(['id','name','categoryType'])->where('id', '>', $latestPost)->get();

        if($ok && $catIds)
            return ['ok' => $ok, 'ids' => $catIds];
        return false;
    }

    public function updateCategory() {

    }

    public function destroy($cat) {
        $ok = Category::find($cat->delCat)->delete();
        if($ok)
            return true;
        return false;
    }

}
