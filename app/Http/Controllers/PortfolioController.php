<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\PostCategory;
use Carbon\Carbon;
use Mavinoo\LaravelBatch\Batch;
use Illuminate\Support\Arr;

class PortfolioController extends Controller
{
    //
    public function __construct() {
//        $this->middleware();
    }

    public function saveCategory(Request $request) {
        if($request->ajax()) {

            $ok = Category::create([
                "name" => $request->categoryName,
                "categoryType" => $request->categoryType,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
            if($ok) {
                return response()->json(["category" => $ok], 200);
            }else {
                return response()->json("fail", 422);
            }
        }
    }


    public function saveLinks(Request $request) {
        if($request->ajax()) {
            $ok = Post::create([
                "userId" => auth()->id(),
                "type" => $request->postType,
                "page" => $request->pageName,
                "link" => $request->link,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
            if($ok)
                return response()->json(["post" => $ok],200);
            return response()->json("fail",422);

        }
    }

    public function check(Request $request, Post $postAll) {
        if($request->ajax()) {
            $categories = Category::where('categoryType', $request->catType)->get();
            $posts = Post::where("type", $request->postType)->get();
            $a = array();
            foreach($posts as $p) {
                $a[] = $p->id;
            }

            $relation = PostCategory::whereIn('postId', $a)->get();
//          dd($relation);
//            $b = array();
//            foreach($posts as $p) {
//                $a[] = $p->id;
//            }
//            $relations = $postAll->findMany($a);
//            foreach($relations as $step => $item) {
//                $b[] = $item->categories()->get();
//                $result = $b[$step][0];
//            }
//
//            dd($relation);
            if(count($categories) > 0 || count($posts) > 0 || count($relation) > 0)
                return response()->json(["category" => $categories, "posts" => $posts, "relation" => $relation], 200);
            return response()->json("empty", 422);

        }
    }

    public function destroyCategory(Request $request) {
        if($request->ajax()) {
            $ok = Category::find($request->delId)->delete();
            if($ok)
                return response()->json("deleted", 200);
            return response()->json("fail", 422);

        }
    }

    public function destroyLink(Request $request) {
        if($request->ajax()) {
            $ok = Post::find($request->delId)->delete();
            if($ok)
                return response()->json("deleted", 200);
            return response()->json("fail", 422);
        }
    }

    public function storeRelation(Request $request, Batch $batch) {
        if($request->ajax()) {

            if(PostCategory::select('id')->first() != null) {
                $lastId = PostCategory::select('id')->orderBy('id', 'DESC')->first()->id;
            }else {
                $lastId = 0;
            }

//            $instance = new PostCategory;
//            $columns = [
//               "postId",
//               "catId",
//               "created_at",
//               "updated_at"
//            ];
            $values = array();
            $arrInLoop = array();
            $notNull = isset($request->postId) ? true : false;
            if($notNull) {
                foreach(json_decode($request->catId) as $categoryId) {
                    $arrInLoop["postId"] = $request->postId;
                    $arrInLoop["catId"] = $categoryId;
                    $arrInLoop["created_at"] = Carbon::now();
                    $arrInLoop["updated_at"] = Carbon::now();
                    $values[] = $arrInLoop;
                }
            }
            $has = PostCategory::select('*')->where("postId", $request->postId)->get();
            if(count($has)) {
                $ids = array();
                foreach($has as $count => $item) {
                    $ids[] = $item->id;
                }
                PostCategory::whereIn('id', $ids)->delete();
            }
            $ok = PostCategory::insert($values);
//            $size = 500;
//            $ok = $batch->insert($instance, $columns, $values, $size);
            if($ok)
                return response()->json(['relation' => PostCategory::select('*')->where('id', '>', $lastId)->get()], 200);
            return response()->json("fail", 422);
        }
    }

    public function filterPortfolio(Request $request) {
        if($request->ajax()) {
            $ok = Category::with("posts")->whereIn('id', json_decode($request->filterIds))->get();
            $paginate = Category::with("posts")->whereIn('id', json_decode($request->filterIds))->paginate(4);
//            $paginate = Category::with("posts")->whereIn('id', json_decode($request->filterIds));

            if($ok)
                return response()->json(["posts" => $ok, "test" => View::make(''), "pegination" => compact($paginate)->render() ], 200);
            return response()->json("fail", 419);
        }
    }

//    public function search(Request $request) {
//        if($request->ajax()) {
//            $finded = Category::with("post")->where('categoryType', 'portfolio')->where('name', 'like', "%{$request->get('q')}%")->get();
//            if($finded)
//                return response()
//        }
//    }

}
