<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Post extends Model
{
//    protected $fillable = ['userId','name','nameEn','nameRu','content','contentRu','contentEn', 'status', 'type', 'page', 'image', 'link'];
    protected $guarded = [];
    protected $table = 'posts';
    public $timestamps = true;

    public function categories() {
        return $this->belongsToMany('App\Category', 'postcategory', 'postId', 'catId')->withTimestamps();
//        return $this->belongsToMany('App\Category')->using('App\PostCategory');
    }

    public static function byId($id) {
        $p = parent::findOrFail($id);
        return $p;
    }


    public static function checkById($id) {
        return parent::where('id', $id)->first();
    }
    public static function checkByTypeAndPage($type, $page) {
        return parent::where('type', $type)->where("page", $page)->first();
    }

    public static function allPosts($pageName) {
//        $userId = Auth::user()->id;
        $all = parent::where('page', $pageName)->get();
        return $all;
    }

}
