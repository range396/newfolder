<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Catorder extends Model
{
    //
    protected $fillable = ['name', 'nameRu', 'nameEn', 'created_at', 'updated_at'];
    public $timestamps = true;
    protected $table = "ordercategories";


    public function getCategoriesBy($name = null, $id = null) {
        if(isset($id)) {
            return $this->find($id)->first();
        }
        if(isset($name)) {
            $searchQuery = trim($name);
            $requestData = ['name', 'nameRu', 'nameEn'];
            $result = $this->where(function($q) use($requestData, $searchQuery) {
                foreach ($requestData as $field)
                    $q->orWhere($field, 'like', "%{$searchQuery}%");
            })->get();
            return $result;
        }
    }
}
